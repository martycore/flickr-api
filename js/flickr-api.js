$( document ).ready(function() {

	var flickerAPI = "http://api.flickr.com/services/feeds/photos_public.gne?jsoncallback=?";

	$.getJSON( flickerAPI, {
		format: "json"
	})

	.done(function( data ) {	

		$.each( data.items, function( i, item ) {

			var html = [
			    '<div class="item">',
			    	'<div class="inner">',
			    		'<a href="' + item.link + '">',
			        		'<div class="thumb"><img src="' + item.media.m + '" /></div>',
		        		'</a>',
			        	'<div class="title"><h3><a href="' + item.link + '">' + item.title, "</a> by " + '<a class="author" href="https://www.flickr.com/people/' + item.author_id + '/">' + item.author + '</a></h3></div>',		        	
			        	'<div class="description">' + item.description + '</div>',
			        	'<div class="tags">' + item.tags + '</div>',
			    	'</div>',
			    '</div>'
			].join("\n");

			$("#flickr-api").append(html);

			if ( i === 11 ) {
				return false;
			}

		});

	});

});